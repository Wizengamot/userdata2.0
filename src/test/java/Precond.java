import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Precond {

    WebDriver driver;
    @BeforeClass
    public static void mainPrecondition(){
        System.setProperty("webdriver.gecko.driver", "C:\\Maven\\geckodriver-v0.26.0-win64\\geckodriver.exe");
    }

    @Before
    public void preCondition(){
        driver = new FirefoxDriver();
    }

    int random1 = (int) (Math.random() * 10000000);
    String randomMobilePhone = "38050" + random1;

    int random2 = (int) (Math.random() * 1000);
    String randomEmail = "alva" + random2 + "@gmail.com";

    /*@After
    public void after() throws InterruptedException {
        Thread.sleep(2000);
        driver.quit();
    }*/
}
