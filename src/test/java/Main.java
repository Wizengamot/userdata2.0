import org.junit.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.net.URL;
import java.util.concurrent.TimeUnit;


public class Main extends Precond {
    final String URL = "https://user-data.hillel.it/html/registration.html";

    @Test
    public void createNewUser() throws InterruptedException {
        open(URL);
        clickTabRegistration();
        FirstName("Alehandro");
        LastName("Alvares");
        workPhone();
        phone();
        email();
        password();
        male();
        position();
        clickButtonRegistration();
        Thread.sleep(1500);
        acceptAlert();
        open(URL);
        fillFieldEmail();
        fillFieldPassword();
        clickButtonAuthorization();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        clickTabMyInfo();

        String checkMobilePhone = driver.findElement(By.id("mobile_phone_my_info")).getAttribute("value");
        Assert.assertEquals(randomMobilePhone, checkMobilePhone);

        String checkEmail = driver.findElement(By.id("email_MyInfo")).getAttribute("value");
        Assert.assertEquals(randomEmail, checkEmail);
    }

    public void open (String url){
        driver.get(URL);
    }
    public void clickTabRegistration(){
        driver.findElement(By.className("registration")).click();
    }
    public void FirstName(String firstName){
        driver.findElement(By.id("first_name")).sendKeys(firstName);
    }
    public void LastName(String lastName){
        driver.findElement(By.id("last_name")).sendKeys(lastName);
    }
    public void workPhone(){
        driver.findElement(By.id("field_work_phone")).sendKeys("7778877");
    }
    public void phone(){
        driver.findElement(By.id("field_phone")).sendKeys(randomMobilePhone);
    }
    public void email(){
        driver.findElement(By.id("field_email")).sendKeys(randomEmail);
    }
    public void password(){
        driver.findElement(By.id("field_password")).sendKeys("Aleha12345");
    }
    public void male(){
        driver.findElement(By.id("male")).click();
    }
    public void position(){
        driver.findElement(By.cssSelector("#position>option[value='qa']")).click();
    }
    public void clickButtonRegistration(){
        driver.findElement(By.id("button_account")).click();
    }
    public void acceptAlert(){
        driver.switchTo().alert().accept();
    }
    public void fillFieldEmail(){
        driver.findElement(By.id("email")).sendKeys(randomEmail);
    }
    public void fillFieldPassword(){
        driver.findElement(By.id("password")).sendKeys("Aleha12345");
    }
    public void clickButtonAuthorization(){
        driver.findElement(By.className("login_button")).click();
    }
    public void clickTabMyInfo(){
        driver.findElement(By.cssSelector("#info")).click();
    }
}
